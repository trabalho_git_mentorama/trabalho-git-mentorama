import java.util.Scanner;

public class Aula_5 {

    //Contempla as aulas 5, 6 e 7 do módulo 2.

    public static void main(String[] args) {

        int opcao;

        do{

            System.out.println("CALCULADORA SIMPLES");
            System.out.println("");
            System.out.println("1 - Somar");
            System.out.println("2 - Subtrair");
            System.out.println("3 - Multiplicar");
            System.out.println("4 - Dividir");
            System.out.println("0 - Sair");

            Scanner scanner = new Scanner(System.in);
            
            opcao = scanner.nextInt();

            processar(opcao);

            if (opcao == 0) scanner.close();

        } while (opcao != 0);


    }

    public static void processar(int opcao) {

        Scanner scanner = new Scanner(System.in);

        switch(opcao) {

            case 1: {

                System.out.println("SOMANDO DOIS NÚMEROS");
                
                System.out.println("Digite o primeiro número: ");
                int numero1 = scanner.nextInt();

                System.out.println("Digite o segundo número: ");
                int numero2 = scanner.nextInt();

                int soma = numero1 + numero2;

                System.out.println("A soma dos dois números é: " + soma);
                
                break;

            }

            case 2: {

                System.out.println("SUBTRAINDO DOIS NÚMEROS");
                
                System.out.println("Digite o primeiro número: ");
                int numero1 = scanner.nextInt();

                System.out.println("Digite o segundo número: ");
                int numero2 = scanner.nextInt();

                int subtracao = numero1 - numero2;

                System.out.println("A subtração dos dois números é: " + subtracao);
                
                break;

            }

            case 3: {

                System.out.println("MULTIPLICANDO DOIS NÚMEROS");
                
                System.out.println("Digite o primeiro número: ");
                int numero1 = scanner.nextInt();

                System.out.println("Digite o segundo número: ");
                int numero2 = scanner.nextInt();

                int multiplicacao = numero1 * numero2;

                System.out.println("A multiplicação dos dois números é: " + multiplicacao);
                
                break;

            }

            case 4: {

                System.out.println("DIVIDINDO DOIS NÚMEROS");
                
                System.out.println("Digite o primeiro número: ");
                double numero1 = scanner.nextInt();

                System.out.println("Digite o segundo número: ");
                double numero2 = scanner.nextInt();

                if (numero2 == 0) {

                    System.out.println("Impossível dividir por 0!");

                } else {

                    double divisao = numero1 / numero2;

                    System.out.println("A divisão dos dois números é: " + divisao);

                }

                break;

            }

        }

        if (opcao == 0) scanner.close();

    }
    
}
